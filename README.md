# Approximating algorithms for Minimum Dominating Set

# Requirements

1. you should install CGAL.
You can find more help [here](https://www.cgal.org/download.html).
2. install pip packages by `pip install -r requirements.txt`.

# How to run

`python main.py`